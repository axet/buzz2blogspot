#!/usr/bin/python

import os
import sys
import json

from xml.dom.minidom import Document

try:
    sys.path.append(os.path.join(os.path.dirname(__file__), './json2xml'))
    import json2xml
except ImportError:
    print 'Error importing json2xml library!!!'

a = sys.stdin.readlines()

doc = Document()

buzz = doc.createElement("buzz")

elem = doc.createElement("post")
for s in a:
  j = json.loads(s)
  json2xml.parse_element(doc, elem, j)
buzz.appendChild(elem)

doc.appendChild(buzz)

print doc.toprettyxml(encoding="utf-8", indent="  ")
